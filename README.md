# MIAGE MPROJ

## Plan du cours

### Séance 1: Découverte de l'entreprise
- Présentation du cours
#### Introduction
- Faire connaissance
- Qu'attendez-vous de ce cours
#### Cours: Découverte de l'entreprise
- Rappel sur la gestion de projets
- Video sur l'entreprise liberé
- Definition d'une entreprise
- Faire un organigramme
- Definir le management de projet
- Lecture du rapport Chaos
#### TP
- Constitution des équipes
- Mon projet
- La céréal Box

### Séance 2: Conceptualiser un projet
#### Introduction
- Entreprendre aujourd'hui
- Video d'Emmanuelle Duez
#### Cours: Lean Canvas & pitcher un projet
- Notion de concept
- L'elevator pitch
- Pitcher son projet
- Video sur le lean canvas
- Le lean canvas partie 1
#### TP
- Réaliser le lean canvas de son projet


### Séance 3: Modéliser un projet
#### Introduction
- Savoir se présenter en entreprise
- Retour sur la ceral box
- La note du public
#### Cours: Découpage, epic et User story
- MVP: Minimum Viable Product
- Lean Start-up
- Les epics
- Les user stories
- Les features
- Split poker: découpage de user stories
#### TP
- Réaliser le MVP de son projet et le découpage en US et features

### Séance 4: Chiffrage 
#### Introduction
- Onboarding d'un membre
- La note du public
- Retour sur le lean canvas
#### Cours: Estimer et chiffrer son projet
- Chiffrage vs estimation
- Points de conplexité
- Poker Planning
- Extreme Quotation
- No estimate
- La confiance d'un chiffrage
- L'Abbaque
#### TP
- Réaliser l'abbaque de chiffrage

### Séance 5: Roadmap
#### Introduction
- Pas de sujet d'intro (temps sur le tp)
- La note du public sur MVP et découpage
- Retour sur le Découpage
#### Cours: Roadmap et priorités
- L'interet d'une roadmap
- Roadmap traditionnelle
- lean roadmap

#### TP
- Réaliser la roadmap de votre projet
- Préparer un COMEX


### Séance 6: COMEX

### Séance 7: Staffing RH

### Séance 8: Kickoff

### Séance 9: Animation d'atelier & brainstorm

### Séance 10: Retrospective du cours


## les outils

- brainstorm
- DOD / DOR
- MOSCOW

## sujet d'intro
- negocier son salaire
- participation au communauté (conf ...)
- montage financier d'une entreprise
- propriété du code
- importance du rsx
- plan du MBA  -> la valeur + le marketing +la vente+ la distribution + le financement
- le manement 3.0
- Communication l'image que l'on souhaite donner


## a classer
rapport Chaos -> agilité